import "react-native";
import React, { useState } from 'react'
import LoginScreen from "../../../../src/screens/Login/Login";
import RNSecureKeyStore, { ACCESSIBLE } from "react-native-secure-key-store";
import { MyAccountnNavigationProp } from "../../../../src/navigation/types";
import { useNavigation } from "@react-navigation/native";
import {
  render,
  screen,
  fireEvent,
  renderHook,
  cleanup
} from "@testing-library/react-native";
import { View, Text, TouchableOpacity, TextInput, Alert } from "react-native";
import Enzyme from "enzyme";
import { mount, shallow } from 'enzyme'
import Adapter from "enzyme-adapter-react-16";
import renderer, { act, create } from "react-test-renderer";

Enzyme.configure({ adapter: new Adapter() });
const mockedNavigate = jest.fn();
jest.mock('@react-navigation/native', () => {
    const actualNav = jest.requireActual('@react-navigation/native');
    return {
        ...actualNav,
        useNavigation: () => ({
        navigate: mockedNavigate,
        }),
    };
});
describe("Login component", () => {
    let wrapper:any;
    const useStateMock = jest.spyOn(React, 'useState')
    beforeEach(() => {
        wrapper = Enzyme.shallow(<LoginScreen />);
        const navigation = useNavigation<MyAccountnNavigationProp>();
    });

    afterEach(() => {
        jest.clearAllMocks();
        afterEach(cleanup);
    });
    it("LoginScreen rendered", () => {
        render(<LoginScreen props />);
    });
    it("Username should not be entered", () => {
        const userName = "Test";
        expect(userName.length).toBeGreaterThan(0);
    });
    it("Password should not be entered", () => {
        const password = "Test@123";
        expect(password.length).toBeGreaterThan(0);
    });
    it("userName should not be null", () => {
        const userName = "";
        expect(userName).not.toBeNull();
    });
    it("Password should not be null", () => {
        const password = "";
        expect(password).not.toBeNull();
    });
    it ('Form submition', () => {
        const onSubmit = jest.fn();
        render(
            <TouchableOpacity onPress={(text) => onSubmit(text)}>
            <Text>Login</Text>
            </TouchableOpacity>
          );
        fireEvent.press(screen.getByText('Login'));
        expect(onSubmit).toBeCalled()

    })
});
