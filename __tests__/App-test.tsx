/**
 * @format
 */

import 'react-native';
import React from 'react';
import App from '../App';
import RootNavigator from '../src/navigation';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
jest.useFakeTimers();

describe('App component', () => {
  it('renders APP component correctly', () => {
      const tree = renderer.create(
          <App />
      ).toJSON();
      expect(tree).toMatchSnapshot();
  });
  it('render RootNavigator correctly', () => {
    const tree = renderer.create(
        <RootNavigator />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});
});