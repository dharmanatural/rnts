import { StyleSheet, Text, View } from 'react-native';

const MyAccountStyles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        alignItems: 'center',
        height: '100%',
        justifyContent: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    formButton: {
        backgroundColor: 'blue',
        height: 50,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    formButtonLabel: {
        color: '#000',
        fontSize: 20,
        paddingBottom: 20
    },
    loginLabel: {
        color: '#fff',
        fontSize: 14
    },
    authName: {
        color: 'blue',
        fontSize: 25,
    },
    authView: {
        width: '100%', 
        justifyContent: 'center', 
        alignItems: 'center' 
    }
})

export {MyAccountStyles}