import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, Alert } from "react-native";
import { MyAccountStyles } from "./style";
import { useNavigation } from "@react-navigation/native";
import { MyAccountnNavigationProp } from "../../navigation/types";
import RNSecureKeyStore, { ACCESSIBLE } from "react-native-secure-key-store";

const MyAccountScreen = () => {
  const navigation = useNavigation<MyAccountnNavigationProp>();
  const [isAuth, setAuth] = useState(false);
  const [userName, setuserName] = useState(false);
  useEffect(() => {
    const unsubscribe = navigation.addListener("focus", () => {
      RNSecureKeyStore.get("username").then((res) => {
        setuserName(res)
      },
      (err) => {
        console.log(err);
      });
      RNSecureKeyStore.get("password").then(
        (res) => {
          setAuth(true);
        },
        (err) => {
          console.log(err);
        }
      );
    });
    return unsubscribe;
  });

  const doLogout = () => {
    RNSecureKeyStore.remove("password").then((res) => {
      setAuth(false);
      setuserName(res);
    })
  }
  return (
    <View style={MyAccountStyles.container}>
      {isAuth ? (
        <View style={MyAccountStyles.authView}>
          <Text style={MyAccountStyles.formButtonLabel}>
            <Text style={MyAccountStyles.authName}>{userName}</Text> You are successfully authenticated!
          </Text>
          <TouchableOpacity style={MyAccountStyles.formButton} onPress={() => doLogout()}>
            <Text style={MyAccountStyles.loginLabel}>Logout</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity
          style={MyAccountStyles.formButton}
          onPress={() =>
            !isAuth &&
            navigation.navigate("Login", {
              name: "Login",
            })
          }
        >
          <Text style={MyAccountStyles.loginLabel}>Login</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default MyAccountScreen;
