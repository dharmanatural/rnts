import * as React from 'react';
import {View, Text} from 'react-native';

const ShopScreen = () => {
  return (
    <View style={{flex: 1, paddingTop: 12, paddingHorizontal: 10}}>
      <Text style={{fontSize: 18}}>Shop Screen</Text>
    </View>
  );
};

export default ShopScreen;
