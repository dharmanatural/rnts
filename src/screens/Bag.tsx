import * as React from 'react';
import {View, Text} from 'react-native';

const BagScreen = () => {
  return (
    <View style={{flex: 1, paddingTop: 12, paddingHorizontal: 10}}>
      <Text style={{fontSize: 18}}>Bag Screen</Text>
    </View>
  );
};

export default BagScreen;
