import { StyleSheet } from "react-native";
const loginStyles = StyleSheet.create({

    container: {
       flexDirection: 'column',
       alignItems: 'center',
       height: '100%',
       justifyContent: 'center'
    },
    formInput: {
        width: '80%',
        height: 50,
        borderWidth: 1,
        borderColor: 'lightgrey',
        marginBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
    },
    formButton: {
        backgroundColor: 'blue',
        height: 50,
        width: '80%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    formButtonLabel: {
        color: '#fff',
        fontSize: 14
    }
       
});

export { loginStyles }
