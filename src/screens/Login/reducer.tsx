import {LoginActionTypes} from './action_types';
import {Action, State} from '../../redux/reducers/index';

const initialState: State = {fetching: false, data: null, err: null};

export const getLogin = (state : State = initialState, action: Action) => {
    switch (action.type) {
        case LoginActionTypes.GET_LOGIN_REQUEST:
          return {
            fetching: true,
            data: null,
            err: null,
          };
        case LoginActionTypes.GET_LOGIN_SUCCESS:
          return {
            fetching: false,
            data: action.payload.data,
            err: null,
          };
        case LoginActionTypes.GET_LOGIN_FAIL:
          return {
            fetching: false,
            data: null,
            err: action.payload.err,
          };
        default:
          return state;
      }
}