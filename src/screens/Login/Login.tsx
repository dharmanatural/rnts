import React, { useState, useEffect } from "react";
import { View, Text, TouchableOpacity, TextInput, Alert } from "react-native";
// import { useDispatch } from "react-redux";
import { loginStyles } from "./style";
import { useNavigation } from "@react-navigation/native";
import RNSecureKeyStore, {ACCESSIBLE} from "react-native-secure-key-store";
import { MyAccountnNavigationProp } from "../../navigation/types";

const LoginScreen = (props: any) => {
  const navigation = useNavigation<MyAccountnNavigationProp>();
  const [userName, setUsername] = useState('');
  const [password, serPassword] = useState('');
  const onSubmit = () => {
    let obj = {userName: userName, password: password}
    RNSecureKeyStore.set("username", userName, {accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY})
    RNSecureKeyStore.set("password", password, {accessible: ACCESSIBLE.ALWAYS_THIS_DEVICE_ONLY})
    .then((res) => {
        navigation.navigate('MyAccount') 
    }, (err) => {
        console.log(err);
    });
  }
  
  
  return (
    <View style={loginStyles.container}>
      <TextInput
        testID='#username'
        style={loginStyles.formInput}
        onChangeText={(text) => setUsername(text)}
        value={userName}
        placeholder="Username"
      />
      <TextInput
        style={loginStyles.formInput}
        onChangeText={(text) => serPassword(text)}
        value={password}
        secureTextEntry={true}
        placeholder="Password"
      />
      <TouchableOpacity
        style={loginStyles.formButton}
        onPress={() => userName?.length > 0 && password?.length > 0 ? onSubmit() : null}
        // onPress={props.handleSubmit(onSubmit)}
      >
        <Text style={loginStyles.formButtonLabel}>Login</Text>
      </TouchableOpacity>
    </View>
  );
};

export default LoginScreen;
// function handleSubmit() {
//   throw new Error("Function not implemented.");
// }
