import {put, takeLatest} from 'redux-saga/effects';
// import {userLogin} from 'api/login';
 
function* loginUser({payload}) {
 try {
    if(payload?.userName?.length > 0 && payload?.password?.length > 0) {
      const response = true;
      yield put({type: 'LOGIN_SUCCESS', response});
    } else {
      const response = false;
      yield put({type: 'LOGIN_FAILURE', response});
    }
 } catch (error) {
   yield put({type: 'LOGIN_FAILURE', error: 'error.message'});
 }
}
export default function* loginScreenSaga() {
 yield takeLatest('LOGIN_REQUEST', loginUser);
}
