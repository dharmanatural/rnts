import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import HomeScreen from '../screens/HomeScreen';
import DetailsScreen from '../screens/DetailsScreen';
import MyAccount from '../screens/MyAccount/MyAccount';
import Login from '../screens/Login/Login';
import {HomeStackNavigatorParamList, MyAccountStackNavigatorParamList} from './types';

const HomeStack = createNativeStackNavigator<HomeStackNavigatorParamList>();
const MyAccountStack = createNativeStackNavigator<MyAccountStackNavigatorParamList>();

const HomeStackNavigator = () => {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name="Discover" component={HomeScreen} />
      <HomeStack.Screen name="Details" component={DetailsScreen} />
    </HomeStack.Navigator>
  );
};

const MyAccountStackNavigator = () => {
  return(
    <MyAccountStack.Navigator>
      <MyAccountStack.Screen name="MyAccount" component={MyAccount}/>
      <MyAccountStack.Screen name="Login" component={Login}/>
    </MyAccountStack.Navigator>
  )
}

const AppStacks = {
  HomeStackNavigator,
  MyAccountStackNavigator
}

export default AppStacks;
