import type { NativeStackNavigationProp } from '@react-navigation/native-stack';
import type { RouteProp } from '@react-navigation/native';

export type HomeStackNavigatorParamList = {
    Discover: undefined;
    Details: {
        name: string;
        birthYear: string;
    };
};

export type MyAccountStackNavigatorParamList = {
    MyAccount: undefined;
    Login: {
        name: string;
    }
}

export type BottomTabNavigatorParamList = {
    Discover: HomeStackNavigatorParamList;
    Shop: undefined;
    Bag: undefined;
    MyAccount: MyAccountStackNavigatorParamList;
}
export type HomeScreenNavigationProp = NativeStackNavigationProp<
    HomeStackNavigatorParamList,
    'Details'
>;

export type DetailsScreenRouteProp = RouteProp<
    HomeStackNavigatorParamList,
    'Details'
>;

export type MyAccountnNavigationProp = NativeStackNavigationProp<
MyAccountStackNavigatorParamList,
    'Login'
>;

export type LoginScreenRouteProp = RouteProp<
MyAccountStackNavigatorParamList,
    'Login'
>;

