import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {BottomTabNavigatorParamList} from './types';
import AppStacks from './AppStack';
import BagScreen from '../screens/Bag';
import MyAccountScreen from '../screens/MyAccount/MyAccount';
import ShopScreen from '../screens/Shop';

const Tab = createBottomTabNavigator<BottomTabNavigatorParamList>();

const BottomTabs = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Discover"
        component={AppStacks.HomeStackNavigator}
        options={{headerShown: false}}
      />
      <Tab.Screen name="Shop" component={BagScreen} />
      <Tab.Screen name="Bag" component={ShopScreen} />
      <Tab.Screen name="MyAccount" options={{headerShown: false}} component={AppStacks.MyAccountStackNavigator} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
