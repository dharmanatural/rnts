import {all} from 'redux-saga/effects';
import loginScreenSaga from '../../screens/Login/saga';

export default function* rootSaga() {
  yield all([loginScreenSaga()]);
}